var divs = document.querySelectorAll("div")
console.log(divs)
divs[0].classList.add("container")
divs[1].classList.add("menu")

function add_circle(){
    let circ = document.createElement("div")
    circ.classList.add("circle")
    circ.style.backgroundColor = rand_color()
    let rad  = rand_radius()
    circ.style.width = rad+"px"
    circ.style.height = rad+"px"
    console.log(rad)

    let top =  Math.floor(Math.random()*(300-rad))
    // console.log(top)
    let left = Math.floor(Math.random()*(800-rad))
    // console.log(left)
    // console.log("=====")
    circ.style.top = top +"px"
    circ.style.left = left +"px"
    divs[0].insertBefore(circ, divs[0].firstChild)
    // divs[0].appendChild(circ)
    // if(divs[0].children.length==0){
    //     divs[0].appendChild(circ)
    // }else{
    //     let chl = divs[0].children
    //     console.log(chl)
    //     divs[0].insertBefore(circ, chl[0])
    // }
}

function rand_color(){
    let r = Math.floor(Math.random()*256)
    let g = Math.floor(Math.random()*256)
    let b = Math.floor(Math.random()*256)
    return "rgb("+r+","+g+","+b+")"
}

function rand_radius(){
    return 2*(Math.floor(Math.random()*46)+5)
}