function task1_1(t){
    document.write("<i><b><u>"+t+"</u></b></i>")
}

function task1_1_2(t){
    document.write("<div style='font-weight:bold; font-style:italic; text-decoration:underline; '>"+t+"</div>")
}

task1_1_2("GAU")

document.write("<hr>")



function f_ex_1() {
    document.write("Hello Gio <br>")
}

f_ex_1()
f_ex_1()
f_ex_1()

function f_ex_2(name, lastname) {
    document.write("Hello "+name+" "+lastname)
}

f_ex_2("Luka", "Tsabadze")

document.write("<hr>")

function f_ex_3(n){
    document.write("<hr>")
    if(n<10){
        document.write("Less 10<br>")
    }else if(n<100){
        document.write("Between 10 and 100<br>")
    }else{
        document.write("Graten 100<br>")
    }
}

f_ex_3(7)
f_ex_3(17)

function f_ex_4() {
    for(i=9; i<20; i+=2){
        document.write("For LOOP - "+i+"<br>")
    }
    document.write("<hr>")
    k=10
    while(k<77){
        document.write("While ->"+k+"<br>")
        k += 10
    }
}

f_ex_4()


