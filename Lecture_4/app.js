const person = {
    name:"Nika",
    'lastName':"Chargeishvili",
    "age":18,
    education:{
        university:"Georgian American University",
        gpa:4.2,
        subject:"Web Programming JS"
    },
    hobbies:["fishing", "travelling", "basketball", "reading"],
    getFullName:function(){
        console.log(this.name, this.lastName)
    }
}

console.log(person)
console.log(person.lastName)
console.log(person['lastName'])
console.log(person.education.gpa)
console.log(person.hobbies[1])
person.getFullName()

person.name = "Nikusha"
console.log(person)
