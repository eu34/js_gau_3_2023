var field = document.querySelector("#field")

function start_draw(){
    setInterval(draw_square, 1000)
}

function draw_square(squares=18){
    console.log(squares)
    field.innerHTML = ""
    for(let i=0; i<squares; i++){
        var sq_el = document.createElement("div")
        sq_el.classList.add("square")
        sq_el.style.backgroundColor = get_random_color();
        field.appendChild(sq_el)
    }
}

function get_random_color(){
    var r =  Math.floor(Math.random()*256);
    var g =  Math.floor(Math.random()*256);
    var b =  Math.floor(Math.random()*256);
    // var color = "rgb("+r+","+g+","+b+")"
    color = `rgb(${r}, ${g}, ${b})`
    return color
}